package com.wildadventures.msaventure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsAventureApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsAventureApplication.class, args);
	}
}
